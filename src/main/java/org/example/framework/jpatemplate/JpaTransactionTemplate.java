package org.example.framework.jpatemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.jpatemplate.exception.JpaTransactionException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

@Slf4j
@RequiredArgsConstructor
public class JpaTransactionTemplate {
  private final EntityManagerFactory emf;

  public <T> T executeInTransaction(final Callback<T> callback) {
    EntityManager em = null;
    EntityTransaction tx = null;

    try {
      em = emf.createEntityManager();
      tx = em.getTransaction();
      tx.begin();
      log.debug("transaction begin");
      final T result = callback.execute(em);
      tx.commit();
      log.debug("transaction commit");

      return result;
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw new JpaTransactionException(e);
    } finally {
      if (em != null) {
        em.close();
      }
    }
  }
}
