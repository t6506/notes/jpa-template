package org.example.framework.jpatemplate;

import javax.persistence.EntityManager;

public interface Callback<T> {
  T execute(final EntityManager em);
}
